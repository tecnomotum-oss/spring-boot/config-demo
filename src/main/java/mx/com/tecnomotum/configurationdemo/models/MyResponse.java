package mx.com.tecnomotum.configurationdemo.models;

public class MyResponse {

    private String jsonEnv;
    private String env;

    public String getJsonEnv() {
        return jsonEnv;
    }

    public void setJsonEnv(String in) {
        this.jsonEnv = in;
    }

    public String getEnv() {
        return env;
    }

    public MyResponse jsonEnv(String value) {
        this.jsonEnv = value;
        return this;
    }

    public MyResponse env(String value) {
        this.env = value;
        return this;
    }
}
