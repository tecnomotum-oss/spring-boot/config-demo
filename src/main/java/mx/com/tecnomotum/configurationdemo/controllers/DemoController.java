package mx.com.tecnomotum.configurationdemo.controllers;

import mx.com.tecnomotum.configurationdemo.models.MyResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.invoke.MethodHandles;

/**
 * Todas las propiedades serán obtenidas del archivo `application.properties`.
 * Esto es obligatorio, si no está en el archivo, fallará la compilación.
 *
 *
 */
@RestController
public class DemoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    /**
     * Orden 4, desde linea de comandos
     *
     * El valor default es `No definida`, sobreescrito desde la linea de comandos:
     * <code>
     * java -jar app.jar --demo.cli="valor"
     * </code>
     */
    @Value("${demo.cli}")
    private String demoCli;

    /**
     * Orden 5, desde la variable de entorno SPRING_APPLICATION_JSON
     *
     * El valor default es <pre>&lt;Definir en SPRING_APPLICATION_JSON&gt;</pre>, sobreescrito desde la variable de entorno
     * <code>
     * SPRING_APPLICATION_JSON='{"demo":{"json":{"env":"valor"}}}' java -jar app.jar
     * </code>
     */
    @Value("${demo.json.env}")
    private String demoJsonEnv;

    /**
     * Orden 10, desde variables de entorno DEMO_EV
     *
     * El valor no está definido en application
     */
    @Value("${DEMO_ENV}")
    private String demoEnv;

    @RequestMapping(path = "/hello")
    public MyResponse sayHello() {
        return new MyResponse()
            .jsonEnv(demoJsonEnv)
            .env(demoEnv);
    }
}
